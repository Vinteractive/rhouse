<div class="wrap fl-settings-network-admin">
    
    <h2 class="fl-settings-heading">
        <?php if(FLBuilderModel::get_branding_icon() != '') : ?>
        <img src="<?php echo FLBuilderModel::get_branding_icon(); ?>" />
        <?php endif; ?>
        <span><?php echo FLBuilderModel::get_branding() . ' ' . __('Settings', 'fl-builder'); ?></span>
    </h2>
    
    <?php if(!empty($_POST) && !isset($_POST['email'])) : ?>
    <div class="updated">
        <p><?php _e('Settings updated!', 'fl-builder'); ?></p>
    </div>
    <?php endif; ?>
    
	<div class="fl-settings-nav">
        <ul>
            <li><a href="#license"><?php _e('License', 'fl-builder'); ?></a></li>
            <li><a href="#modules"><?php _e('Modules', 'fl-builder'); ?></a></li>
            <li><a href="#templates"><?php _e('Templates', 'fl-builder'); ?></a></li>
            <li><a href="#post-types"><?php _e('Post Types', 'fl-builder'); ?></a></li>
            <li><a href="#editing"><?php _e('Editing', 'fl-builder'); ?></a></li>
            <li><a href="#branding"><?php _e('Branding', 'fl-builder'); ?></a></li>
            <li><a href="#uninstall"><?php _e('Uninstall', 'fl-builder'); ?></a></li>
        </ul>
	</div>
	
	<div class="fl-settings-content">

	    <!-- LICENSE -->
        <div id="fl-license-form" class="fl-settings-form">
            <?php do_action('fl_themes_license_form'); ?>
        </div>
        <!-- LICENSE -->
	
	    <!-- MODULES -->
        <div id="fl-modules-form" class="fl-settings-form">
    
            <h3 class="fl-settings-form-header"><?php _e('Enabled Modules', 'fl-builder'); ?></h3>
            
            <form id="modules-form" action="<?php echo network_admin_url('/settings.php?page=fl-builder-multisite-settings#modules'); ?>" method="post">
                
                <div class="fl-settings-form-content">
        
                    <p><?php _e('Check or uncheck modules below to enable or disable them.', 'fl-builder'); ?></p>
                    <?php 
                    
                    $enabled_modules = FLBuilderModel::get_enabled_modules();
                    $checked = in_array('all', $enabled_modules) ? 'checked' : '';
                        
                    ?>
                    <label>
                        <input class="fl-module-all-cb" type="checkbox" name="fl-modules[]" value="all" <?php echo $checked; ?> />
                        <?php _e('All', 'fl-builder'); ?>
                    </label>
                    <?php 
                    
                    foreach(FLBuilderModel::$modules as $module) : 
                    
                        $checked = in_array($module->slug, $enabled_modules) ? 'checked' : '';
                        
                    ?>
                    <p>
                        <label>
                            <input class="fl-module-cb" type="checkbox" name="fl-modules[]" value="<?php echo $module->slug; ?>" <?php echo $checked; ?> />
                            <?php echo $module->name; ?>
                        </label>
                    </p>
                    <?php endforeach; ?>
                </div>
                <p class="submit">
                    <input type="submit" name="update" class="button-primary" value="Save Module Settings" />
                    <?php wp_nonce_field('modules', 'fl-modules-nonce'); ?>
                </p>
            </form>
        </div>
        <!-- MODULES -->
	
	    <!-- TEMPLATES -->
        <div id="fl-templates-form" class="fl-settings-form">
    
            <h3 class="fl-settings-form-header"><?php _e('Template Settings', 'fl-builder'); ?></h3>
            
            <form id="templates-form" action="<?php echo network_admin_url('/settings.php?page=fl-builder-multisite-settings#templates'); ?>" method="post">

                <div class="fl-settings-form-content">
                        
                    <p><?php _e('Enable or disable templates using the options below.', 'fl-builder'); ?></p>
                    <?php
                    
                    $enabled_templates = FLBuilderModel::get_enabled_templates();
                    
                    ?>
                    <select name="fl-template-settings">
                        <option value="enabled" <?php selected($enabled_templates, 'enabled'); ?>><?php _e('Enable All Templates'); ?></option>
                        <option value="core" <?php selected($enabled_templates, 'core'); ?>><?php _e('Enable Core Templates Only'); ?></option>
                        <option value="user" <?php selected($enabled_templates, 'user'); ?>><?php _e('Enable User Templates Only'); ?></option>
                        <option value="disabled" <?php selected($enabled_templates, 'disabled'); ?>><?php _e('Disable All Templates'); ?></option>
                    </select>
                </div>                    
                <p class="submit">
                    <input type="submit" name="update" class="button-primary" value="Save Template Settings" />
                    <?php wp_nonce_field('templates', 'fl-templates-nonce'); ?>
                </p>
            </form>
        </div>
        <!-- TEMPLATES -->
	
	    <!-- POST TYPES -->
        <div id="fl-post-types-form" class="fl-settings-form">
    
            <h3 class="fl-settings-form-header"><?php _e('Post Types', 'fl-builder'); ?></h3>
            
            <form id="post-types-form" action="<?php echo network_admin_url('/settings.php?page=fl-builder-multisite-settings#post-types'); ?>" method="post">
                <div class="fl-settings-form-content">
                    <p><?php _e('Enter a comma separated list of the post types you would like the builder to work with.', 'fl-builder'); ?></p>
                    <p><?php _e('NOTE: Not all custom post types may be supported.', 'fl-builder'); ?></p>
                    <?php
                    
                    $saved_post_types = FLBuilderModel::get_post_types();
                    
                    foreach($saved_post_types as $key => $post_type) {
                        if($post_type == 'fl-builder-template') {
                            unset($saved_post_types[$key]);
                        }
                    }
                    
                    $saved_post_types = implode(', ', $saved_post_types);
                    
                    ?>
                    <input type="text" name="fl-post-types" value="<?php echo esc_html($saved_post_types); ?>" class="regular-text" />
                    <p class="description"><?php _e('Example: page, post, product', 'fl-builder'); ?></p>
                </div>
                <p class="submit">
                    <input type="submit" name="update" class="button-primary" value="Save Post Types" />
                    <?php wp_nonce_field('post-types', 'fl-post-types-nonce'); ?>
                </p>
            </form>
        </div>
        <!-- POST TYPES -->
	
	    <!-- EDITING -->
        <div id="fl-editing-form" class="fl-settings-form">
    
            <h3 class="fl-settings-form-header"><?php _e('Editing Settings', 'fl-builder'); ?></h3>
            
            <form id="editing-form" action="<?php echo network_admin_url('/settings.php?page=fl-builder-multisite-settings#editing'); ?>" method="post">
                <div class="fl-settings-form-content">
                    
                    <p><?php echo sprintf(__('Set the <a href="%s" target="_blank">capability</a> required for users to access advanced builder editing such as adding, deleting or moving modules.', 'fl-builder'), 'http://codex.wordpress.org/Roles_and_Capabilities#Capability_vs._Role_Table'); ?></p>
                    
                    <input type="text" name="fl-editing-capability" value="<?php echo esc_html(FLBuilderModel::get_editing_capability()); ?>" class="regular-text" />
                    
                </div>
                <p class="submit">
                    <input type="submit" name="update" class="button-primary" value="Save Editing Settings" />
                    <?php wp_nonce_field('editing', 'fl-editing-nonce'); ?>
                </p>
            </form>
        </div>
        <!-- EDITING -->
        
        <?php if(file_exists(FL_BUILDER_DIR . 'includes/admin-branding.php')) : ?>
        <!-- BRANDING -->
        <?php include FL_BUILDER_DIR . 'includes/admin-branding.php'; ?>
        <!-- BRANDING -->
        <?php endif; ?>
        
        <!-- UNINSTALL -->
        <div id="fl-uninstall-form" class="fl-settings-form">
    
            <h3 class="fl-settings-form-header"><?php _e('Uninstall', 'fl-builder'); ?></h3>

            <p><?php _e('Clicking the button below will uninstall the page builder plugin and delete all of the data associated with it. You can uninstall or deactivate the page builder from the plugins page instead if you do not wish to delete the data.', 'fl-builder'); ?></p>
            
            <p><strong><?php _e('NOTE:'); ?></strong> <?php _e('The builder does not delete the post meta _fl_builder_data, _fl_builder_draft and _fl_builder_enabled in case you want to reinstall it later. If you do, the builder will rebuild all of its data using those meta values.', 'fl-builder'); ?></p>
            
            <?php if(is_multisite()) : ?>
            <p><strong style="color:#ff0000;"><?php _e('NOTE:'); ?></strong> <?php _e('This applies to all sites on the network.', 'fl-builder'); ?></p>
            <?php endif; ?>
            
            <form id="uninstall-form" action="<?php echo network_admin_url('/settings.php?page=fl-builder-multisite-settings'); ?>" method="post">
                <p>
                    <input type="submit" name="uninstall-submit" class="button button-primary" value="<?php _e('Uninstall', 'fl-builder'); ?>">
                    <?php wp_nonce_field('uninstall', 'fl-uninstall'); ?>
                </p>
            </form>
        </div>
        <!-- UNINSTALL -->
        
	</div>
</div>            
<script type="text/javascript">

jQuery(function(){
    
    FLBuilderAdminSettings.strings = {
        uninstall: '<?php _e('Please type "uninstall" in the box below to confirm that you really want to uninstall the page builder and all of its data.', 'fl-builder'); ?>'
    };
});

</script>