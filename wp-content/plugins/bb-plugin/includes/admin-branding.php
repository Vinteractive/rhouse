<div id="fl-branding-form" class="fl-settings-form">

    <h3 class="fl-settings-form-header"><?php _e('Branding', 'fl-builder'); ?></h3>

    <?php if(is_network_admin()) : ?>
    <form action="<?php echo network_admin_url('/settings.php?page=fl-builder-multisite-settings#branding'); ?>" method="post">
    <?php else : ?>
    <form action="<?php echo admin_url('/options-general.php?page=fl-builder-settings#branding'); ?>" method="post">
    <?php endif; ?>

        <p><?php _e('White label the page builder by entering a custom name below.', 'fl-builder'); ?></p>
        <input type="text" name="fl-branding" value="<?php echo esc_html(FLBuilderModel::get_branding()); ?>" class="regular-text" />

        <p><?php _e('Additionally, you may also add a custom icon by entering the URL of an image below. Leave the field blank if you do not wish to use an icon.', 'fl-builder'); ?></p>
        <input type="text" name="fl-branding-icon" value="<?php echo esc_html(FLBuilderModel::get_branding_icon()); ?>" class="regular-text" />
        
        <p class="submit">
            <input type="submit" name="update" class="button-primary" value="Save Branding" />
            <?php wp_nonce_field('branding', 'fl-branding-nonce'); ?>
        </p>
    </form>

</div>