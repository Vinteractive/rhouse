<form class="fl-contact-form">

    <div class="fl-input-group fl-name">
        <label for="fl-name"><?php _e('Name', 'fl-builder');?></label>
        <span class="fl-contact-error"><?php _e('Please enter your name.', 'fl-builder');?></span>
        <input type="text" name="fl-name" value="" placeholder="Your name" />
    </div>

    <div class="fl-input-group fl-email">
        <label for="fl-email"><?php _e('Email', 'fl-builder');?></label>
        <span class="fl-contact-error"><?php _e('Please enter a valid email.', 'fl-builder');?></span>
        <input type="email" name="fl-email" value="" placeholder="Your email" />
    </div>

    <div class="fl-input-group fl-message">
        <label for="fl-message"><?php _e('Your Message', 'fl-builder');?></label>
        <span class="fl-contact-error"><?php _e('Please enter a message.', 'fl-builder');?></span>
        <textarea name="fl-message" placeholder="Your message"></textarea>
    </div>
        
    <input type="text" value="<?php echo $settings->mailto_email; ?>" style="display: none;" class="fl-mailto">

    <input type="submit" value="<?php echo __('Send', 'fl-builder'); ?>" class="fl-contact-form-submit" />
    <span class="fl-success" style="display:none;">Message Sent!</span>
    <span class="fl-send-error" style="display:none;">Message failed. Please try again.</span>
</form>