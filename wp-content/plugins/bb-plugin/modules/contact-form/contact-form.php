<?php

/**
 * @class FLHtmlModule
 */
class FLContactFormModule extends FLBuilderModule {

    /**
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'           => __('Contact Form', 'fl-builder'),
            'description'    => __('A very simple contact form.', 'fl-builder'),
            'category'       => __('Advanced Modules', 'fl-builder'),
            'editor_export'  => false
        ));

        add_action('wp_ajax_fl_builder_email', array($this, 'send_mail'));
        add_action('wp_ajax_nopriv_fl_builder_email', array($this, 'send_mail'));
    }

    /**
     * @method send_mail
     */
    static public function send_mail($params = array()) {

        // Get the contact form post data
        $name = (isset($_POST['name']) ? $_POST['name'] : null);
        $email = (isset($_POST['email']) ? $_POST['email'] : null);
        $message = (isset($_POST['message']) ? $_POST['message'] : null);
        $mailto = (isset($_POST['mailto']) ? $_POST['mailto'] : null);

        // Email template/subject
        $subject  = __('Contact Form Submission', 'fl-builder');
        $template = "Name: $name \r\n" .
                    "Email: $email \r\n" .
                    "Message: \r\n" .
                    $message;

        // Double check the mailto email is proper and send
        if ($mailto && filter_var($email, FILTER_VALIDATE_EMAIL)) {
            wp_mail($mailto, $subject, $template);
            die('1');
        } else {
            die($mailto);
        }
    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('FLContactFormModule', array(
    'general'       => array(
        'title'         => __('General', 'fl-builder'),
        'sections'      => array(
            'general'       => array(
                'title'         => '',
                'fields'        => array(
                    'mailto_email'     => array(
                        'type'          => 'text',
                        'label'         => __('Send To Email', 'fl-builder'),
                        'default'       => '',
                        'placeholder'   => __('example@mail.com', 'fl-builder'),
                        'help'          => __('The contact form will send to this e-mail', 'fl-builder'),
                        'preview'       => array(
                            'type'          => 'none'
                        )
                    ),
                )
            )
        )
    )
));