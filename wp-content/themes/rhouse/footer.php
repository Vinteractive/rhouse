<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package progression
 * @since progression 1.0
 */
?>
<div class="clearfix"></div>
</div><!-- close #main -->


<?php if( is_page_template('homepage.php') ): ?>
	<?php if ( is_active_sidebar( 'homepage-widgets' ) ) : ?>
		<?php dynamic_sidebar( 'homepage-widgets' ); ?>
	<?php endif; ?>
<?php endif; ?>

<?php if ( is_active_sidebar( 'homepage-all-widgets' ) ) : ?>
	<?php dynamic_sidebar( 'homepage-all-widgets' ); ?>
<?php endif; ?>

<div id="widget-area">
	<div class="width-container footer-<?php echo get_theme_mod('footer_cols', '4'); ?>-column">
		<?php if ( ! dynamic_sidebar( 'Footer Widgets' ) ) : ?>
		<?php endif; // end sidebar widget area ?>
	</div>
	<div class="clearfix"></div>
</div>

<footer>
	<div id="copyright">
		<div class="width-container">
			
			<div class="footer-3-column">
				<div class="widget widget_text">
					<img align="left" src="<?php do_shortcode('wpurl'); ?>/wp-content/uploads/2014/12/rhouse-logo.png">
				</div> 	
			</div>

			<div class="footer-3-column">
				<div class="widget widget_text">
					<span align="center">Copyright &copy; RHouse <?php echo get_the_time('Y'); ?> All Rights Reserved. </span>
				</div>
			</div>

			<div class="footer-3-column">
				<div class="widget widget_text">
					<span align="right"><?php echo get_theme_mod( 'copyright_textbox', 'Developed by Vinteractive' ); ?></span>
				</div>
			</div>
				
		</div><!-- close .width-container -->
		<div class="clearfix"></div>
	</div><!-- close #copyright -->
</footer>
<?php wp_footer(); ?>
</body>
</html>